
function displayTime(){
    $("#date").text("");
    $("#time").text("");
    const optDate = {weekday:"short", month:"short", year:"numeric", day:"numeric"};
    $("#date").append(`${Intl.DateTimeFormat("en-US", optDate).format(Date.now())}`);
    const optTime = {hour: 'numeric', minute: 'numeric', second: 'numeric'};
    $("#time").append(`${Intl.DateTimeFormat("en-US", optTime).format(Date.now())}`);
}

const createClock = setInterval(displayTime, 1000);

function login(){
    $("#errMsg").text("");
    const email = $("#emailInput").val();
    console.log(email);
    const password = $("#pswdInput").val();
    console.log(password);
    const emailPattern = /.+?@.+?\..+?/;
    console.log("email pattern matched?: "+ email.match(emailPattern));
    if(email.match(emailPattern) === null || password.length < 6){
        let errorMessage = "<span>Error! Please complete the form!</span>";
        const emailErr = "<br><span>* Email address nust be filled in!</span>";
        const pswdErr = "<br><span>* Password length must be at least 6 characters!</span>";
        if(email === "" ) errorMessage += emailErr;
        else if(email.match(emailPattern)  === null) errorMessage += "<br><span>* Email format is incorrect.</span>";
        if(password.length < 6 ) errorMessage += pswdErr;
        $("#errMsg").append(errorMessage);
    }else{
        fetchForecast();
    }
}

async function fetchForecast(){
    let url = `http://dataservice.accuweather.com/forecasts/v1/daily/5day/56186?apikey=GQ6FrAonxa3b32w75jrUKLE7fh3ZSHVt&metric=true`;
    try{
        const response = await fetch(url);
        const profile = await response.json();
        $("#forecast").text("");
        let forecastHTML = "<p>Weather in Montreal for the next 5 days!</p>";
        const forecastArray = profile.DailyForecasts;
        console.log(forecastArray);
        for(let daily of forecastArray){
            forecastHTML += `<li> <a href=\"${daily.Link}\">${daily.Date}</a><br>`;
            forecastHTML += `<p>Max: ${daily.Temperature.Maximum.Value}${daily.Temperature.Maximum.Unit} Min: ${daily.Temperature.Minimum.Value}${daily.Temperature.Minimum.Unit}</p>`;
            forecastHTML += `<p>Day: ${daily.Day.IconPhrase} Night: ${daily.Night.IconPhrase}</p> </li>`;
        }
        $("#forecast").append(forecastHTML);
    }catch(err){
        console.log(err);
        alert("SOMETHING WRONG HERE, PLEASE RELOAD...");
    }
    
}