
let student1 = {name: "Bach", studentId: 10000001};
let student2 = {name: "Mozart", studentId: 10000002};
let student3 = {name: "Beethoven", studentId: 10000003};
let student4 = {name: "Chopin", studentId: 10000004};
let student5 = {name: "Debussy", studentId: 10000005};
let stdArray = [student1, student2, student3, student4, student5];

function addElement(id, name){
    let nameEl = document.createElement("li");
    nameEl.innerHTML = `${id} : ${name} `;
    //nameEl.setAttribute("id", `${id}`);
    nameEl.id = `${id}`;// same as setAttribute
    nameList.appendChild(nameEl);
    let dltEl = document.createElement("span");
    dltEl.innerHTML = `&lt;&lt;delete`;
     //dltEl.setAttribute("class", "deleteItem");
    dltEl.className = `deleteItem`; // same as setAttribute
    dltEl.onclick = deleteItem;
    nameEl.appendChild(dltEl);
}

const nameList = document.getElementById("nameList");
for(let item of stdArray){
    addElement(item.studentId, item.name);
}

const lessEl = document.getElementById("lessView");
const moreEl = document.getElementById("moreView");
const inputEl = document.getElementById("nameToAdd");
const studentEl = document.getElementById('stdSelected');

inputEl.addEventListener("keyup", function(event) {
                                    if (event.key === "Enter") {
                                        event.preventDefault();
                                        document.getElementById("add-btn").click();
                                    }
                                  });

//const scrollEventHandler =  (event)=>{event.preventDefault();document.getElementById("select-btn").click();};
//document.addEventListener("scroll", scrollEventHandler);//only works when scroll bar works

function viewLess(){
    nameList.style.display = "none";
    lessEl.style.display = "none";
    moreEl.style.display = "block";
}

function viewMore(){
    nameList.style.display = "block";
    lessEl.style.display = "block";
    moreEl.style.display = "none";
}

let namePattern = /[\!@#\$%\^&\*()+=\\\|\?\<\>\d]/;
let upperLetterStartPattern = /^[A-Z].+?$/;

function addName(){
    let nameAdded = inputEl.value;
    //console.log(typeof nameAdded);
    let nonLetterMatches = nameAdded.match(namePattern);
    let upperLetterMatches = nameAdded.match(upperLetterStartPattern);
    //console.log(nonLetterMatches);
    //console.log(upperLetterMatches);
    if(nameAdded!==""){
        if(!nonLetterMatches){
            if(upperLetterMatches){
                let index = stdArray.length;
                stdArray.push({name:nameAdded, studentId: stdArray[index-1].studentId+1});
                console.log(stdArray);
                addElement(stdArray[index-1].studentId+1, nameAdded);
                inputEl.value = '';
            }else{
                alert("Name must starts with upper letter.");
            }
        }else{
            alert("Student name must not include digits or symbols other than -.")
        }
    }else{
        alert("Student name must be entered.")
    }
}

let showName = true;
let generateCount = 0;

function getStudentRandomly(){
    let randomIndex = Math.floor(Math.random()*stdArray.length);
    const orderEl = document.getElementById("order");
    orderEl.innerHTML = `#${++generateCount} Round Selection: `;
    if(showName===true){
        studentEl.value = stdArray[randomIndex].name;
    }else{
        studentEl.value = stdArray[randomIndex].studentId;
    }
}

function clear(){
    const studentEl = document.getElementById('stdSelected');
    studentEl.value = "";
}

function toShowName(){
    if(showName===false){
        showName = true;
        clear();
    }
}

function toShowID(){
    if(showName===true){
        showName = false;
        clear();
    }
}

function deleteItem(evt){
    //evt.target.parentElement.style.display = "none";
    evt.target.parentElement.remove();
    let cntnt = evt.target.parentElement.textContent;
    let ctnArray = cntnt.split(" ");
    console.log(ctnArray);
    let index = -1;
    for(let std of stdArray){
        if(std.studentId === Number(ctnArray[0])) index = stdArray.indexOf(std);
    }
    stdArray.splice(index, 1);
    console.log(stdArray);
}