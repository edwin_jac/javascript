//get elements of input and output
const billAmountEl = document.getElementById("billamt");
const serviceQualEl = document.getElementById("serviceQual");
const peopleAmountEl = document.getElementById("peopleamt");
const totalAndTipEl = document.getElementById("totalAndTip");

//Calculate Tip
function calculateTip() {
    totalAndTipEl.innerHTML = '';
    //step 1: Fetch values from web elements
    const billAmount = billAmountEl.value;
    console.log("billAmount: "+billAmount);
    const serviceCharge = serviceQualEl.options[serviceQualEl.selectedIndex].value;
    console.log("serviceCharge: "+serviceCharge);
    const peopleAmount = peopleAmountEl.value;
    console.log("peopleAmount: "+peopleAmount);
    const numberBillAmount = Number(billAmount);
    const numberServiceCharge = Number(serviceCharge);
    // have to use let instead of const because it might be changed in step 3
    let numberPeopleAmount = Number(peopleAmount); 
    //step 2: Validate if bill amount and service charge has been entered or not. If not show an alert saying "Please enter values".
    if(billAmount === "" || serviceCharge === "0" ){
        alert("Please enter values");
    }else{
        //step 3: For number of people if nothing is entered or a negative value is entered assume that there is only one person paying the bill.
        //missing request of serviceCharge = 0 here. using the same logic negative number for edge criteria
        if(peopleAmount === "" || numberPeopleAmount <= 0 ){
            numberPeopleAmount = 1;
            console.log("corrected serviceCharge: "+numberPeopleAmount);
        } 
        //step 4: Calculate tip
        const numberTip = numberBillAmount * numberServiceCharge;
        console.log("tip: "+numberTip);
        //step 5: Calculate Total bill (per person)
        const numberTotalBill = (numberBillAmount + numberTip) / numberPeopleAmount;
        console.log("total: "+numberTotalBill);
        //step 6 and 7: Display tip on html page and Display total on html page 
        const formatter = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD'});
        const fragment = document.createDocumentFragment();
        const tipEl = document.createElement("p");
        tipEl.textContent = `Amount of Tip: ${formatter.format(numberTip)}`;
        const totalEl = document.createElement("p");
        totalEl.textContent = `Total: ${formatter.format(numberTotalBill)} per person`;
        fragment.appendChild(tipEl);
        fragment.appendChild(totalEl);
        totalAndTipEl.appendChild(fragment);
        //step 8 and 9
        luckydraw();
    } 
}

//fetch toy name and display it in alert of step 8 & 9
async function luckydraw(){
    let url = "https://swapi.dev/api/people/20/";
    try{
        let response = await fetch(url);
        console.log(response);
        let profile = await response.json();
        console.log(profile);
        alert(`You have won a ${profile.name} toy`);
    }catch(err){
        console.log(err);
        alert("SOMETHING WRONG HERE, PLEASE RELOAD...");
    }
}