console.log("JS works fine.");

function changeBackgroundColorDark(){
    console.log("dark button is clicked");
    const mainEl = document.getElementById("main");
    mainEl.style.backgroundColor = 'darkgray';
}
function changeBackgroundColorLight(){
    console.log("light button is clicked");
    const mainEl = document.getElementById("main");
    mainEl.style.backgroundColor = 'white';
}