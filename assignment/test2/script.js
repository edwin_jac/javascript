// array of food list and carolies
let foodArray = [];
const foodList = document.getElementById("foodList");
const foodInput = document.getElementById("foodName");
const caroliesInput = document.getElementById("foodCalories");

//event listener of submit button
function addFood(){
    //strings of food name and calories
    let newFood = foodInput.value;
    let newCarolies = caroliesInput.value;
    //save new item and sort the array by carolies
    foodArray.push({food: newFood, carolies: newCarolies});
    foodArray.sort((x,y)=>x.carolies>y.carolies?1:x.carolies<y.carolies?-1:0);
    console.log(foodArray);//display whole array on console
    //create fragment with all list items added
    const fragment = document.createDocumentFragment();
    for(let f of foodArray){
        let foodListItem = document.createElement("li");
        foodListItem.innerHTML = `${f.food} - ${f.carolies}`;
        fragment.appendChild(foodListItem);
    }
    //clear child nodes of <ul> and append fragment of new sorted items
    foodList.innerHTML = '';
    foodList.appendChild(fragment);
    //clear the inputs
    foodInput.value = '';
    caroliesInput.value = '';
}