const nameList = document.getElementById("names");
console.log(nameList.hasChildNodes());
console.log(nameList.childNodes);
console.log(nameList);
const inputEl = document.getElementById("nameToAdd");
//console.log(inputEl);

function addName(){
    let nameAdded = inputEl.value;
    //console.log(nameAdded);
    let nameEl = document.createElement("p");
    nameEl.innerHTML = `${nameAdded}<br>`;
    console.log(nameEl);

    nameList.appendChild(nameEl);
    console.log(nameList);

    inputEl.value = '';
}

/*
    <h1>Reading value from text box</h1>
    <h3> List of Names</h3>
    <div id="names"></div>
    <label for="nameToAdd">Enter a Name: </label>
    <input typ="text" id="nameToAdd" default="a new name">
    <button id="my-btn" onclick="addName()">Add a Name</button>
*/
