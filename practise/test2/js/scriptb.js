let clickCount = 0;
let el = document.querySelector("#headline");


//find out "Hello World"
/*
<div id="headline">
    BEFORE 
    <h1>Hello World </h1>
    AFTER 
</div>
*/

let nodesList = el.childNodes;
console.log(nodesList);
let nodesArray = Array.from(nodesList);//[...nodesList];
nodesArray.forEach(n=>console.log(`${n.innerHTML}`));
nodesArray.forEach(n=>console.log(`${n.textContent}`));

nodesArray.filter(n=>n.innerHTML!==undefined).forEach(n=>console.log(`[${nodesArray.indexOf(n)}] : ${n.innerHTML}`));

el.onclick = function(){
    switch(++clickCount%4){
        case 1:
            nodesArray[1].innerHTML = nodesArray[1].innerHTML.replace(/.$/, '-');
            console.log(el.innerHTML);
            break;
        case 2:
            nodesArray[1].innerHTML = nodesArray[1].innerHTML.replace(/.$/, '\\');
            console.log(el.innerHTML);
            break;
        case 3:
            nodesArray[1].innerHTML = nodesArray[1].innerHTML.replace(/.$/, '|');
            console.log(el.innerHTML);
            break;
        case 0:
            nodesArray[1].innerHTML = nodesArray[1].innerHTML.replace(/.$/, '/');
            console.log(el.innerHTML);
            break;
    }
};

