const mainHeading = document.querySelector('#main-heading');
console.log(mainHeading);
const otherHeading = document.querySelector('#other-heading');
console.log(otherHeading);
const excitedText = document.createElement('span');
excitedText.textContent = '!!!';
let nodeList = mainHeading.childNodes;
for(let n of [...nodeList])
    otherHeading.appendChild(n);//moving ??? from main to other!
console.log(otherHeading);
//console.log(excitedText);
mainHeading.appendChild(excitedText);
console.log(mainHeading);
//otherHeading.appendChild(excitedText); // if adding this line, mainHeading will not have been appended
//console.log(otherHeading);
mainHeading.style.cssText = 'text-decoration: underline;';

/*
<div id="headline">
BEFORE 
<h1>Hello World </h1>
AFTER 
</div>
let el = document.querySelector("#headline");
let nodesList = el.childNodes;
console.log(nodesList);
let nodesArray = Array.from(nodesList);
nodesArray.forEach(n=>console.log(`${n.innerHTML}`));
nodesArray.forEach(n=>console.log(`${n.textContent}`));
*/

let foo=-1
if(-5){
    console.log("true");
}else{
    console.log("false");
}
