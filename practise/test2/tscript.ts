enum Color {
    Red,
    Green,
    Blue = 4,
  }
let c: Color = Color.Green;
console.log(c);
let colorName: string = Color[1];
console.log(colorName);// Displays 'Green'


interface LabeledValue {
  label: string;
}

function printLabel(labeledObj: LabeledValue) {
  console.log(labeledObj.label);
}

let myObj = { size: 10, label: "Size 10 Object" };
printLabel(myObj);
 