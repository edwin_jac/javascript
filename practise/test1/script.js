//console.log("Hello World!");

//alert("It's real! We have the first javascript running!");

var foo = 10;
var bar = 7;

var sum = foo+bar;

console.log("the classical way of \"\": sum is "+sum);
console.log(`the new way of \`\$\{\}\`: sum is ${sum}`);

var s = new Set([7,9,"0"]);
console.log(`size of the set: ${s.size}`);
s.add([1,2,3,5,'4',6]);
console.log(`size of the set: ${s.size}`);
s.add("8");
console.log(`size of the set: ${s.size}`);
console.log(`contains 9?: ${s.has(9)}`);
console.log(`contains 5?: ${s.has(5)}`);
console.log(`contains \"0\"?: ${s.has("0")}`);
s.add("0");
console.log(`size of the set: ${s.size}`);
s.add(0);
console.log(`size of the set: ${s.size}`);

var a = [1,2,3,'4',5,6];
var b = a;
console.log(`log an array: ${a}`);
console.log(`type of a": ${typeof(a)}`);

s.add(a);
console.log(`size of the set: ${s.size}`);
console.log(`contains a?: ${s.has(a)}`);
console.log(`contains b?: ${s.has(b)}`);

var c = [1,2,3,'4',5,6];
console.log(`contains c?: ${s.has(c)}`);
s.add(c);
console.log(`size of the set: ${s.size}`);

console.log(`string with multiplication\"8\"*2: ${"8"*2}`);
console.log(`string with multiplication\"a\"*2: ${"a"*2}`);

console.log(`euqal? \'4\' vs. \"4\": ${'4' == "4"}`);
console.log(`type of \'4\': ${typeof('4')}`);
console.log(`type of \"4\": ${typeof("4")}`);

console.log(`type of null: ${typeof(null)}`);
console.log(`type of NaN: ${typeof(NaN)}`);
console.log(`type of undefined: ${typeof(undefined)}`);

let m = new Map();
m.set(1, a);
console.log(`size of the map: ${m.size}`);
m.set(2, b);
console.log(`size of the map: ${m.size}`);
m.set(3, c);
console.log(`size of the map: ${m.size}`);
m.set(a, 1);
console.log(`size of the map: ${m.size}`);
m.set(b, 2);
console.log(`size of the map: ${m.size}`);
m.set(c, 3);
console.log(`size of the map: ${m.size}`);
console.log([...m]);

console.log(`\'Green\' < \'green\'? ${'Green'<'green'}`);
console.log(`\'a\' < \'b\'? ${'a'<'b'}`);