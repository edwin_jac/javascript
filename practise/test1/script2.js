
var s = 'a';

s = 'string';

switch(s){
    case "a string":
        console.log("case a string");
        break;
    case "two string":
        console.log("case two string");
        break;
    case "string":
        console.log("case string");
        break;
    default:
        console.log("default");
}

console.log('a'==="a"?true:false);

var [a, b, c] = [1,[2,3]];
//var [a, [b,c]] = [1,2,3]; //TypeError
console.log(a+" "+b+" "+c);

/*
var eatsPlants = true;
var eatsAnimals = false;
var category = 
eatsPlants? (eatsAnimals? "omnivore":"herbivore"):(eatsAnimals? "carnivore":"undefined");

//(eatsPlants && eatsAnimals)? "omnivore": 
//(!eatsPlants && eatsAnimals)? "carnivore": 
//(eatsPlants && !eatsAnimals)? "herbivore": 
//"undefined";


console.log(category);
*/
/*
var number = 27;

if(number%3===0 && number%5===0){
    console.log(`number ${number} can be divided by both 3 and 5`);
}else{
    console.log(`number ${number} cannot be divided by both 3 and 5`);
}
*/
/*
//var shirtWidth = 18; 
//var shirtLength = 28; 
//var shirtSleeve = 8.13; 
var shirtWidth = 18; 
var shirtLength = 29; 
var shirtSleeve = 8.47; 
var shirtSize;
if(shirtWidth>=18 && shirtWidth<20){
    shirtSize = 'S';
}else if(shirtWidth>=20 && shirtWidth<22){
    shirtSize = 'M';
}else if(shirtWidth>=22 && shirtWidth<24){
    shirtSize = 'L';
}else if(shirtWidth>=24 && shirtWidth<26){
    shirtSize = 'XL';
}else if(shirtWidth>=26 && shirtWidth<28){
    shirtSize = '2XL';
}else if(shirtWidth>=28){
    shirtSize = '3XL';
}else{
    shirtSize = 'NA';
}
if(shirtLength>=28 && shirtLength<29 && shirtSize == 'S'){
    shirtSize = 'S';
}else if(shirtLength>=29 && shirtLength<30 && shirtSize == 'M'){
    shirtSize = 'M';
}else if(shirtLength>=30 && shirtLength<31 && shirtSize == 'L'){
    shirtSize = 'L';
}else if(shirtLength>=31 && shirtLength<33 && shirtSize == 'XL'){
    shirtSize = 'XL';
}else if(shirtLength>=33 && shirtLength<34 && shirtSize == '2XL'){
    shirtSize = '2XL';
}else if(shirtLength>=34 && shirtSize == '3XL'){
    shirtSize = '3XL';
}else{
    shirtSize = 'NA';
}
if(shirtSleeve>=8.13 && shirtSleeve<8.38 && shirtSize == 'S'){
    shirtSize = 'S';
}else if(shirtSleeve>=8.38 && shirtSleeve<8.63 && shirtSize == 'M'){
    shirtSize = 'M';
}else if(shirtSleeve>=8.63 && shirtSleeve<8.88 && shirtSize == 'L'){
    shirtSize = 'L';
}else if(shirtSleeve>=8.88 && shirtSleeve<9.63 && shirtSize == 'XL'){
    shirtSize = 'XL';
}else if(shirtSleeve>=9.63 && shirtSleeve<=10.13 && shirtSize == '2XL'){
    shirtSize = '2XL';
}else if(shirtSleeve>=10.13 && shirtSize == '3XL'){
    shirtSize = '3XL';
}else{
    shirtSize = 'NA';
}
console.log(shirtSize);
*/