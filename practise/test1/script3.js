
var array = [1,2,3]
console.log(array);
array.reverse();
console.log(array);

var str = "this is a string."
var rev = [];
//console.log(str.reverse);
for(let i = str.length-1; i>=0; i--){
    rev += str[i];
    //console.log(str[i]);
}
console.log(rev);

console.log(1==true);// true
console.log(1===true);// false
console.log(4==true);// false
console.log([9]==true);// false

var troop = {a:1, b:2};
console.log(troop.a??".a is not defined");// false
console.log(troop.x??".x is not defined");// false

function power (a, b){
    //console.log(a**b);
    var pw = 1; 
    if(b>=0){
        if((Number(b) === b && b % 1 === 0)){
            for(var i=0; i<b; i++){
                pw *= a;
            }
            return pw;
        }else{
            return "power must be an integer";
        }
    }else{
        return "power cannot be less than 0";
    }
}

var result = power(0.2,5);
console.log(result);

console.log(-5==true);
console.log([]==true);
console.log({}==true);

var x = 1;
console.log(`x= ${x}`);
function addTwo() {
    x = x + 2;
  }
addTwo();
console.log(`x= ${x}`);
function addTwoVar() {
  var x = x + 2;
}
addTwoVar();
console.log(`x= ${x}`);

let y = 1;
console.log(`y= ${y}`);
function addTwoY() {
    y = y + 2;
}
addTwoY();
console.log(`y= ${y}`);
function addTwoVarY() {
  //let y = 2;//ReferenceError
  var y = y+2;
}
addTwoVarY();
console.log(`y= ${y}`);

function addTwoZ() {
    z = z + 2;
  }
//let z = 1;//ReferenceError
var z = 1;
addTwoZ();
console.log(`z= ${z}`);

sayHi("Julia");
function sayHi(name) {
  //var greeting; //undefined Julia
  console.log(greeting + " " + name);
  var greeting = "hi"; //undefined Julia
  //let greeting = "hi"; //ReferenceError: Cannot access 'greeting' before initialization
}