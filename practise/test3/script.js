
let student1 = {name: "George Lucas", studentId: 10000000};
let stdArray = [student1];

function addElement(id, name){
    let nameEl = document.createElement("li");
    nameEl.innerHTML = `${id} : ${name} `;
    //nameEl.setAttribute("id", `${id}`);
    nameEl.id = `${id}`;// same as setAttribute
    nameList.appendChild(nameEl);
    let dltEl = document.createElement("span");
    dltEl.innerHTML = `&lt; delete`;
    //dltEl.setAttribute("class", "deleteItem");
    dltEl.className = `deleteItem`; // same as setAttribute
    dltEl.onclick = deleteItem;
	nameEl.appendChild(dltEl);
	let editEl = document.createElement("span");
    editEl.innerHTML = ` edit &gt;`;
     //dltEl.setAttribute("class", "editItem");
	editEl.className = `editItem`; // same as setAttribute
    editEl.onclick = edtItem;
    nameEl.appendChild(editEl);
}

const nameList = document.getElementById("nameList");
for(let item of stdArray){
    addElement(item.studentId, item.name);
}

const lessEl = document.getElementById("lessView");
const moreEl = document.getElementById("moreView");
const inputEl = document.getElementById("nameToAdd");
const studentEl = document.getElementById('stdSelected');
const fetchEl = document.getElementById('stdFetched');
const searchEl = document.getElementById('stdSearch');
const fetchStarshipEl = document.getElementById('starshipFetched');

inputEl.addEventListener("keyup", function(event) {
                                    if (event.key === "Enter") {
                                        event.preventDefault();
                                        document.getElementById("add-btn").click();
                                    }
                                  });

//const scrollEventHandler =  (event)=>{event.preventDefault();document.getElementById("select-btn").click();};
//document.addEventListener("scroll", scrollEventHandler);//only works when scroll bar works

function viewLess(){
    nameList.style.display = "none";
    lessEl.style.display = "none";
    moreEl.style.display = "block";
}

function viewMore(){
    nameList.style.display = "block";
    lessEl.style.display = "block";
    moreEl.style.display = "none";
}

let namePattern = /[\!@#\$%\^&\*()+=\\\|\?\<\>\d]/;
let upperLetterStartPattern = /^[A-Z].+?$/;

let fetched = false;
let nameFetched = "";

function addName(){
    let nameAdded = inputEl.value;
    //console.log(typeof nameAdded);
    let nonLetterMatches = nameAdded.match(namePattern);
    let upperLetterMatches = nameAdded.match(upperLetterStartPattern);
    //console.log(nonLetterMatches);
    //console.log(upperLetterMatches);
    if(nameAdded!==""){
        if(!nonLetterMatches){
            if(upperLetterMatches){
                let index = stdArray.length;
                stdArray.push({name:nameAdded, studentId: stdArray[index-1].studentId+1});
                console.log(stdArray);
                addElement(stdArray[index-1].studentId+1, nameAdded);
                inputEl.value = '';
            }else{
                alert("Name must starts with upper letter.");
            }
        }else{
            alert("Student name must not include digits or symbols other than -.")
        }
	}else if(fetched === true && nameFetched !== false){
		fetched = false;
		let index = stdArray.length;
        stdArray.push({name:nameFetched, studentId: stdArray[index-1].studentId+1});
        console.log(stdArray);
        addElement(stdArray[index-1].studentId+1, nameFetched);
	}else{
        alert("Student name must be entered.")
    }
}

let showName = true;
let generateCount = 0;

function getStudentRandomly(){
    let randomIndex = Math.floor(Math.random()*stdArray.length);
    const orderEl = document.getElementById("order");
    orderEl.innerHTML = `#${++generateCount} Round Selection: `;
    if(showName===true){
        studentEl.value = stdArray[randomIndex].name;
    }else{
        studentEl.value = stdArray[randomIndex].studentId;
    }
}

function clear(){
    const studentEl = document.getElementById('stdSelected');
    studentEl.value = "";
}

function toShowName(){
    if(showName===false){
        showName = true;
        clear();
    }
}

function toShowID(){
    if(showName===true){
        showName = false;
        clear();
    }
}

function deleteItem(evt){
    //evt.target.parentElement.style.display = "none";
    evt.target.parentElement.remove();
    let cntnt = evt.target.parentElement.textContent;
    let ctnArray = cntnt.split(" ");
    console.log(ctnArray);
    let index = -1;
    for(let std of stdArray){
        if(std.studentId === Number(ctnArray[0])) index = stdArray.indexOf(std);
    }
    stdArray.splice(index, 1);
    console.log(stdArray);
}

function edtItem(){
	console.log("do something here");
}

let fetchCount = 0;

function fetchData(){
	const oReq = new XMLHttpRequest();
    oReq.addEventListener("load", requestListener);
    oReq.addEventListener("error", requestErrorListener);
    let url;
    url = `https://swapi.dev/api/people/${++fetchCount}/`;
    //url = "https://www.microsoft.com"
	console.log(url);
	oReq.open("GET", url);
	oReq.send();
}

function requestListener(){
    if(this.status < 400){
        fetched = true;
        nameFetched = JSON.parse(this.responseText).name;
        console.log(nameFetched);
        fetchEl.value = nameFetched;
        document.getElementById("add-btn").click();
    }else{
        console.log(this);
        fetchEl.value = "ERROR of "+this.status;
    }
}

//only triggered by network-level errors, it ignores HTTP return codes.
function requestErrorListener(){ 
    console.log(this); // url="https://www.microsoft.com" causes net::ERR_FAILED
}

function searchData(){
    let nameToSearch = searchEl.value;
    if(nameToSearch !== ""){
        for(let item of stdArray){
            let pattern = "\\.?"+nameToSearch+"\\.?";
            let searchMatches = item.name.match(pattern);
            if(searchMatches){
                searchEl.value = `${item.studentId} @ ${stdArray.indexOf(item)}`;
                let nodeArray = [...nameList.childNodes];
                //console.log(nodeArray);
                //for(let nodeItem of nodeArray) if(nodeItem.textContent.match(pattern)) console.log(nodeItem.textContent);
                console.log(nodeArray[stdArray.indexOf(item)].textContent);
            }
        }
    }
}

let starshipCount = 0;

async function fetchStarshipData(){
    let url;
    url = `https://swapi.dev/api/starships/${++starshipCount}/`;
    //url = "https://www.microsoft.com" // get error of net::ERR_FAILED
    try{
        let response = await fetch(url);
        console.log(response);
        let profile = await response.json();
        console.log(profile);
        fetchStarshipEl.value = "";
        for(let [a, b] of Object.entries(profile)) fetchStarshipEl.value += a + " : " + b + "\n";
    }catch(err){
        console.log(err);
        fetchStarshipEl.value = "SOMETHING WRONG HERE, PLEASE RELOAD...";
    }
    
}