
let student1 = {name: "George Lucas", studentId: 10000000};
let student2 = {name: "Irvin Kershner", studentId: 10000001};
let stdArray = [student1, student2];

function addElement(id, name){
    let nameEl = document.createElement("li");
    nameEl.innerHTML = `${id} : ${name} `;
    //nameEl.setAttribute("id", `${id}`);
    nameEl.id = `${id}`;// same as setAttribute
    nameList.appendChild(nameEl);
    let dltEl = document.createElement("span");
    dltEl.innerHTML = `&lt; delete`;
    //dltEl.setAttribute("class", "deleteItem");
    dltEl.className = `deleteItem`; // same as setAttribute
    dltEl.onclick = deleteItem;// must set for each added items
	nameEl.appendChild(dltEl);
	let editEl = document.createElement("span");
    editEl.innerHTML = ` edit &gt;`;
     //dltEl.setAttribute("class", "editItem");
	editEl.className = `editItem`; // same as setAttribute
    editEl.onclick = edtItem;
    nameEl.appendChild(editEl);
}

const nameList = document.getElementById("nameList");
for(let item of stdArray){
    addElement(item.studentId, item.name);
}

const lessEl = document.getElementById("lessView");
const moreEl = document.getElementById("moreView");
const inputEl = document.getElementById("nameToAdd");


inputEl.addEventListener("keyup", function(event) {
                                    if (event.key === "Enter") {
                                        event.preventDefault();
                                        document.getElementById("add-btn").click();
                                    }
                                  });

//const scrollEventHandler =  (event)=>{event.preventDefault();document.getElementById("select-btn").click();};
//document.addEventListener("scroll", scrollEventHandler);//only works when scroll bar works

function viewLess(){
    nameList.style.display = "none";
    lessEl.style.display = "none";
    moreEl.style.display = "block";
}

function viewMore(){
    nameList.style.display = "block";
    lessEl.style.display = "block";
    moreEl.style.display = "none";
}

let namePattern = /[\!@#\$%\^&\*()+=\\\|\?\<\>\d]/;
let upperLetterStartPattern = /^[A-Z].+?$/;

let fetched = false;
let nameFetched = "";

function addName(){
    let nameAdded = $("#nameToAdd").val();
    console.log(nameAdded);
    let nonLetterMatches = nameAdded.match(namePattern);
    let upperLetterMatches = nameAdded.match(upperLetterStartPattern);
    //console.log(nonLetterMatches);
    //console.log(upperLetterMatches);
    if(nameAdded!==""){
        if(!nonLetterMatches){
            if(upperLetterMatches){
                let index = stdArray.length;
                stdArray.push({name:nameAdded, studentId: stdArray[index-1].studentId+1});
                console.log(stdArray);
                addElement(stdArray[index-1].studentId+1, nameAdded);
                inputEl.value = '';
            }else{
                alert("Name must starts with upper letter.");
            }
        }else{
            alert("Student name must not include digits or symbols other than -.")
        }
	}else if(fetched === true && nameFetched !== false){
		fetched = false;
		let index = stdArray.length;
        stdArray.push({name:nameFetched, studentId: stdArray[index-1].studentId+1});
        console.log(stdArray);
        addElement(stdArray[index-1].studentId+1, nameFetched);
	}else{
        alert("Student name must be entered.")
    }
}

let showName = true;
let generateCount = 0;

function getStudentRandomly(){
    let randomIndex = Math.floor(Math.random()*stdArray.length);
    const orderEl = document.getElementById("order");
    orderEl.innerHTML = `#${++generateCount} Round Selection: `;
    if(showName===true){
        studentEl.value = stdArray[randomIndex].name;
    }else{
        studentEl.value = stdArray[randomIndex].studentId;
    }
}

function clear(){
    const studentEl = document.getElementById('stdSelected');
    studentEl.value = "";
}

function toShowName(){
    if(showName===false){
        showName = true;
        clear();
    }
}

function toShowID(){
    if(showName===true){
        showName = false;
        clear();
    }
}

$(".deleteItem").on("click", deleteItem);

function deleteItem(event){
    //event.target.parentElement.style.display = "none";
    event.target.parentElement.remove();
    let cntnt = event.target.parentElement.textContent;
    let ctnArray = cntnt.split(" ");
    console.log(ctnArray);
    let index = -1;
    for(let std of stdArray){
        if(std.studentId === Number(ctnArray[0])) index = stdArray.indexOf(std);
    }
    stdArray.splice(index, 1);
    console.log(stdArray);
}

function edtItem(){
	console.log("do something here");
}

//async. fetch data of role and starship
function fetchStarShip(){
    let randomIndex = 1+Math.floor(Math.random()*10);
    let url = `https://swapi.dev/api/starships/${randomIndex}/`;
    $.ajax(url).done(requestStarshipListener).fail(requestStarshipErrorListener);
}

function requestStarshipListener(data){
    let shipName = data.name;
    $("#starShipSelected").val(shipName).css("color", "#818181");
}
function requestStarshipErrorListener(data){
    console.log(data);
    let ErrorMsg = data.status;
    if(ErrorMsg === 404){
        ErrorMsg = "Not Found";
    }
    $("#starShipSelected").val(ErrorMsg).css("color", "red");
}

let fetchCount = 0;

function fetchRole(){
	let url = `https://swapi.dev/api/people/${++fetchCount}/`;
    $.ajax(url).done(requestRoleListener).fail(requestRoleErrorListener);
}

function requestRoleListener(data){
    fetched = true;
    nameFetched = data.name;
    $("#roleFetched").val(nameFetched).css("color", "#818181");
	$("#add-btn").click();
}
function requestRoleErrorListener(data){
    console.log(data);
    let ErrorMsg = data.status;
    if(ErrorMsg === 404){
        ErrorMsg = "Not Found";
    }
    $("#roleFetched").val(ErrorMsg).css("color", "red");

}